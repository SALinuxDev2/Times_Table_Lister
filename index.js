const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });
  
  
class UserInputValues {
    constructor(multiplier, start, end) {
        this.multiplier = multiplier;
        this.start = start;
        this.end = end;
    }

    multiply() {
        for (let i = this.start; i <= this.end; i++) {
            console.log("======== Times Table =======");
            console.log(i + " x " + this.multiplier + " = " + i * this.multiplier);
        } 
    }
}

function running() {
    readline.question("What number do you want to multiply by?", multiplier => {
        readline.question("What is your start number?", start => {
            readline.question("What is your end number?", end => {
                const timesTable = new UserInputValues(`${multiplier}, ${start}, ${end}`);
                timesTable.multiply();
            })
        })
    })
}

running();

